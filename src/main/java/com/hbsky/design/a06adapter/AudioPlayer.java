package com.hbsky.design.a06adapter;

/**
 * 音乐播放器
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 10:10
 */
public class AudioPlayer implements MediaPlayer {

    /**
     * 媒体适配器
     */
    private MediaAdapter mediaAdapter;

    private static final String MP3 = "mp3";
    private static final String VLC = "vlc";
    private static final String MP4 = "mp4";

    @Override
    public void play(String audioType, String fileName) {

        //播放 mp3 音乐文件的内置支持
        if (MP3.equalsIgnoreCase(audioType)) {
            System.out.println("Playing mp3 file. Name: " + fileName);
        }
        //mediaAdapter 提供了播放其他文件格式的支持
        else if (VLC.equalsIgnoreCase(audioType)
                || MP4.equalsIgnoreCase(audioType)) {
            mediaAdapter = new MediaAdapter(audioType);
            mediaAdapter.play(audioType, fileName);
        } else {
            System.out.println("Invalid media. " +
                    audioType + " format not supported");
        }
    }
}