package com.hbsky.design.a06adapter;

/**
 * 播放MP4格式的实现类
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 10:06
 */
public class Mp4Player implements AdvancedMediaPlayer {

    @Override
    public void playVlc(String fileName) {
        //什么也不做
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("Playing mp4 file. Name: " + fileName);
    }
}