package com.hbsky.design.a06adapter;

/**
 * 媒体播放器的适配器类
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 10:07
 */
public class MediaAdapter implements MediaPlayer {

    private static final String VLC = "vlc";
    private static final String MP4 = "mp4";

    private AdvancedMediaPlayer advancedMusicPlayer;

    public MediaAdapter(String audioType) {
        if (audioType.equalsIgnoreCase(VLC)) {
            advancedMusicPlayer = new VlcPlayer();
        } else if (audioType.equalsIgnoreCase(MP4)) {
            advancedMusicPlayer = new Mp4Player();
        }
    }

    @Override
    public void play(String audioType, String fileName) {
        if (audioType.equalsIgnoreCase(VLC)) {
            advancedMusicPlayer.playVlc(fileName);
        } else if (audioType.equalsIgnoreCase(MP4)) {
            advancedMusicPlayer.playMp4(fileName);
        }
    }
}