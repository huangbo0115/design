package com.hbsky.design.a06adapter;

/**
 * 播放vlc格式的实现类
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 10:05
 */
public class VlcPlayer implements AdvancedMediaPlayer {
    @Override
    public void playVlc(String fileName) {
        System.out.println("Playing vlc file. Name: " + fileName);
    }

    @Override
    public void playMp4(String fileName) {
        //什么也不做
    }
}