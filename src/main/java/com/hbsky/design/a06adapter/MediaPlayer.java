package com.hbsky.design.a06adapter;

/**
 * 媒体播放器接口
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 10:01
 */
public interface MediaPlayer {
    /**
     * 播放一般格式
     *
     * @param audioType audioType
     * @param fileName  fileName
     * @author bo.huang update
     * @date 2018/11/14 0014 10:04
     */
    void play(String audioType, String fileName);
}