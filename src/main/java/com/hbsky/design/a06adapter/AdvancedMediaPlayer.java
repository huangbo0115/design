package com.hbsky.design.a06adapter;

/**
 * 高级媒体播放器
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 10:02
 */
public interface AdvancedMediaPlayer {

    /**
     * 播放vlc格式
     *
     * @param fileName fileName
     * @author bo.huang update
     * @date 2018/11/14 0014 10:03
     */
    void playVlc(String fileName);

    /**
     * 播放MP4格式
     *
     * @param fileName fileName
     * @author bo.huang update
     * @date 2018/11/14 0014 10:03
     */
    void playMp4(String fileName);
}