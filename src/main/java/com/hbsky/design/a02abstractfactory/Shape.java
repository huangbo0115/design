package com.hbsky.design.a02abstractfactory;

/**
 * 形状
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:08
 */
public interface Shape {
    void draw();
}