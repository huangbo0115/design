package com.hbsky.design.a02abstractfactory;

/**
 * 绿色
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:09
 */
public class Green implements Color {

    @Override
    public void fill() {
        System.out.println("Inside Green::fill() method.");
    }
}