package com.hbsky.design.a02abstractfactory;

/**
 * 圆形
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:09
 */
public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}