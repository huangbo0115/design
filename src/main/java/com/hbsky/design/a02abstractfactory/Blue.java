package com.hbsky.design.a02abstractfactory;

/**
 * 蓝色
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:09
 */
public class Blue implements Color {

    @Override
    public void fill() {
        System.out.println("Inside Blue::fill() method.");
    }
}