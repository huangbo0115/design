package com.hbsky.design.a02abstractfactory;

/**
 * 颜色
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:09
 */
public interface Color {
    void fill();
}