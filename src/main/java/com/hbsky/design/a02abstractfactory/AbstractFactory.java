package com.hbsky.design.a02abstractfactory;

/**
 * 抽象工厂
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:11
 */
public abstract class AbstractFactory {

    /**
     * 获取颜色抽象实例
     *
     * @param color color
     * @return com.hbsky.design.a02abstractfactory.Color
     * @author bo.huang update
     * @date 2018/11/13 0013 15:13
     */
    public abstract Color getColor(String color);

    /**
     * 获取形状抽象实例
     *
     * @param shape shape
     * @return com.hbsky.design.a02abstractfactory.Shape
     * @author bo.huang update
     * @date 2018/11/13 0013 15:13
     */
    public abstract Shape getShape(String shape);
}