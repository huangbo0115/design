package com.hbsky.design.a02abstractfactory;

/**
 * 正方形
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:09
 */
public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}