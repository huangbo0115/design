package com.hbsky.design.a02abstractfactory;

/**
 * 红色
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:09
 */
public class Red implements Color {

    @Override
    public void fill() {
        System.out.println("Inside Red::fill() method.");
    }
}