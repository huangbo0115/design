package com.hbsky.design.a01factory;

/**
 * 客户端代码只需要将相应的参数传入即可得到对象
 * 用户不需要了解工厂类内部的逻辑。
 * @author bo.huang
 */
public class Client {

    private final static String A = "A";
    private final static String B = "B";

    public Food get(String name) {
        Food x;
        if (A.equals(name)) {
            x = StaticFactory.getA();
        } else if (B.equals(name)) {
            x = StaticFactory.getB();
        } else {
            x = StaticFactory.getC();
        }
        return x;
    }
}