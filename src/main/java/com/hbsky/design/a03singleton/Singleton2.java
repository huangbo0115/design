package com.hbsky.design.a03singleton;

import lombok.extern.java.Log;

/**
 * 懒汉式 单例 线程安全
 * 双检锁/双重校验锁（DCL，即 double-checked locking）
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:32
 */
@Log
public class Singleton2 {
    private volatile static Singleton2 singleton;

    private Singleton2() {
        log.info("创建了Singleton2");
    }

    public static Singleton2 getSingleton() {
        if (singleton == null) {
            synchronized (Singleton2.class) {
                if (singleton == null) {
                    singleton = new Singleton2();
                }
            }
        }
        return singleton;
    }
}