package com.hbsky.design.a03singleton;

import lombok.extern.java.Log;

/**
 * 枚举 实现单例
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:32
 */
@Log
public enum Singleton4 {
    INSTANCE;

    public void whateverMethod() {
    }
}