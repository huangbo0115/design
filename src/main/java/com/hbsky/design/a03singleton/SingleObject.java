package com.hbsky.design.a03singleton;

import lombok.extern.java.Log;

/**
 * 饿汉式 单例模式
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:28
 */
@Log
public class SingleObject {

    /**
     * 创建 SingleObject 的一个对象
     */
    private static SingleObject instance = new SingleObject();

    /**
     * 让构造函数为 private，这样该类就不会被实例化
     */
    private SingleObject() {
        log.info("创建实例SingleObject");
    }

    /**
     * 获取唯一可用的对象
     *
     * @return com.hbsky.design.a03singleton.SingleObject
     * @author bo.huang update
     * @date 2018/11/13 0013 15:29
     */
    public static SingleObject getInstance() {
        return instance;
    }

    /**
     * 获取唯一可用的对象
     *
     * @author bo.huang update
     * @date 2018/11/13 0013 15:29
     */
    public void showMessage() {
        System.out.println("Hello World!");
    }
}