package com.hbsky.design.a03singleton;

import lombok.extern.java.Log;

/**
 * 懒汉式 单例 线程安全
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:32
 */
@Log
public class Singleton1 {
    private static Singleton1 instance;

    private Singleton1() {
        log.info("Singleton1");
    }

    /**
     * 获取实例 (问题:虽然在保证了安全问题,但是影响了效率,应为每次调用此方法的时候都要线程等待,解决:只要在实例为空的时候synchronized就好了)
     *
     * @return com.hbsky.design.a03singleton.Singleton1
     * @author bo.huang update
     * @date 2018/11/13 0013 15:44
     */
    public static synchronized Singleton1 getInstance() {
        if (instance == null) {
            instance = new Singleton1();
        }
        return instance;
    }
}