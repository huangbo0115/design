package com.hbsky.design.a03singleton;

import lombok.extern.java.Log;

/**
 * 懒汉式 单例 线程安全
 * 双检锁/双重校验锁（DCL，即 double-checked locking）
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:32
 */
@Log
public class Singleton3 {
    private static class SingletonHolder {
        private static final Singleton3 INSTANCE = new Singleton3();
    }

    private Singleton3() {
        log.info("创建了Singleton3");
    }

    public static Singleton3 getInstance() {
        return SingletonHolder.INSTANCE;
    }
}