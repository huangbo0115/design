package com.hbsky.design.a03singleton;

/**
 * 懒汉式 单例 线程不安全
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 15:32
 */
public class Singleton {
    private static Singleton instance;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}