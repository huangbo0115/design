package com.hbsky.design.a07bridge;

/**
 * 实现了 Shape 接口的实体类  圆形
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 14:38
 */
public class Circle extends Shape {
    private int x, y, radius;

    public Circle(int x, int y, int radius, DrawAPI drawAPI) {
        super(drawAPI);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    @Override
    public void draw() {
        drawAPI.drawCircle(radius, x, y);
    }
}