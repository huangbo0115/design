package com.hbsky.design.a07bridge;

/**
 * 桥接实现接口
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 14:36
 */
public interface DrawAPI {
    void drawCircle(int radius, int x, int y);
    void add();
}