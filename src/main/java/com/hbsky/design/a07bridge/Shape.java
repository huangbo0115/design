package com.hbsky.design.a07bridge;

/**
 * DrawAPI 接口创建抽象类 Shape。
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 14:38
 */
public abstract class Shape {
    protected DrawAPI drawAPI;

    protected Shape(DrawAPI drawAPI) {
        this.drawAPI = drawAPI;
    }

    public abstract void draw();
}