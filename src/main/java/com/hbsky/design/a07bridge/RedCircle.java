package com.hbsky.design.a07bridge;

/**
 * 桥接实现类 红色
 *
 * @author bo.huang update
 * @date 2018/11/14 0014 14:37
 */
public class RedCircle implements DrawAPI {
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: red, radius: "
                + radius + ", x: " + x + ", " + y + "]");
    }

    @Override
    public void add() {
        System.out.println("接口实现添加功能,不影响shape");
    }
}