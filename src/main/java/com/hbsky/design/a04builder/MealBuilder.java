package com.hbsky.design.a04builder;

/**
 * 点单
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:23
 */
public class MealBuilder {

    /**
     * 素食汉堡+可口可乐 套餐
     *
     * @return com.hbsky.design.a04builder.Meal
     * @author bo.huang update
     * @date 2018/11/13 0013 16:27
     */
    public Meal prepareVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new VegBurger()).addItem(new Coke());
        return meal;
    }

    /**
     * 鸡肉汉堡+百事可乐 套餐
     *
     * @return com.hbsky.design.a04builder.Meal
     * @author bo.huang update
     * @date 2018/11/13 0013 16:28
     */
    public Meal prepareNonVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger()).addItem(new Pepsi());
        return meal;
    }

    /**
     * 自己点单
     *
     * @return com.hbsky.design.a04builder.Meal
     * @author bo.huang update
     * @date 2018/11/13 0013 16:41
     */
    public Meal definedMeal() {
        return new Meal();
    }

}