package com.hbsky.design.a04builder;

/**
 * 瓶子包装
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:12
 */
public class Bottle implements Packing {

    @Override
    public String pack() {
        return "瓶子包装";
    }
}