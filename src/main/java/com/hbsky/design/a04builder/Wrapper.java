package com.hbsky.design.a04builder;

/**
 * 纸盒包装
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:11
 */
public class Wrapper implements Packing {

    @Override
    public String pack() {
        return "纸盒包装";
    }
}