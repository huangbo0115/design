package com.hbsky.design.a04builder;

/**
 * 素食汉堡
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:19
 */
public class VegBurger extends AbstractBurger {

    @Override
    public float price() {
        return 25.0f;
    }

    @Override
    public String name() {
        return "素食汉堡";
    }
}