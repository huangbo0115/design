package com.hbsky.design.a04builder;

/**
 * 食物类目
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:09
 */
public interface Item {
    /**
     * 食物名称
     *
     * @return java.lang.String
     * @author bo.huang update
     * @date 2018/11/13 0013 16:09
     */
    public String name();

    /**
     * 食物包装
     *
     * @return com.hbsky.design.a04builder.Packing
     * @author bo.huang update
     * @date 2018/11/13 0013 16:09
     */
    public Packing packing();

    /**
     * 价格
     *
     * @return float
     * @author bo.huang update
     * @date 2018/11/13 0013 16:17
     */
    public float price();
}