package com.hbsky.design.a04builder;

/**
 * 包装类型
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:10
 */
public interface Packing {
    /**
     * 包装
     *
     * @author bo.huang update
     * @date 2018/11/13 0013 16:10
     */
    public String pack();
}