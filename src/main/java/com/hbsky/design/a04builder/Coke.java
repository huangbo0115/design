package com.hbsky.design.a04builder;

/**
 * 可口可乐
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:21
 */
public class Coke extends AbstractColdDrink {

    @Override
    public float price() {
        return 30.0f;
    }

    @Override
    public String name() {
        return "可口可乐";
    }
}