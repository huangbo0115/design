package com.hbsky.design.a04builder;

/**
 * 冷饮抽象类
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:17
 */
public abstract class AbstractColdDrink implements Item {

    @Override
    public Packing packing() {
        return new Bottle();
    }

    /**
     * 冷饮价格计算
     *
     * @return float
     * @author bo.huang update
     * @date 2018/11/13 0013 16:18
     */
    @Override
    public abstract float price();
}