package com.hbsky.design.a04builder;

/**
 * 百事可乐
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:21
 */
public class Pepsi extends AbstractColdDrink {

    @Override
    public float price() {
        return 35.0f;
    }

    @Override
    public String name() {
        return "百事可乐";
    }
}