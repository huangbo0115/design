package com.hbsky.design.a04builder;

/**
 * 鸡肉汉堡
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:20
 */
public class ChickenBurger extends AbstractBurger {

    @Override
    public float price() {
        return 50.5f;
    }

    @Override
    public String name() {
        return "鸡肉汉堡";
    }
}