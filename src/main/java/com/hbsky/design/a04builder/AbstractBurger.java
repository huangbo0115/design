package com.hbsky.design.a04builder;

/**
 * 汉堡包
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:13
 */
public abstract class AbstractBurger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    /**
     * 汉堡包价格计算抽象
     *
     * @return float
     * @author bo.huang update
     * @date 2018/11/13 0013 16:15
     */
    @Override
    public abstract float price();
}