package com.hbsky.design.a05protype;

/**
 * 图形抽象类 (原型模式)
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 17:25
 */
public abstract class AbstractShape implements Cloneable {

    private String id;
    protected String type;

    abstract void draw();

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}