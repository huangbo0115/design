package com.hbsky.design.a05protype;

import java.util.Hashtable;

/**
 * 缓存.模拟数据库
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 17:26
 */
public class ShapeCache {

    private static Hashtable<String, AbstractShape> shapeMap = new Hashtable<>();

    public static AbstractShape getShape(String shapeId) {
        AbstractShape cachedShape = shapeMap.get(shapeId);
        return (AbstractShape) cachedShape.clone();
    }

    /**
     * // 对每种形状都运行数据库查询，并创建该形状
     * // shapeMap.put(shapeKey, shape);
     * // 例如，我们要添加三种形状
     *
     * @author bo.huang update
     * @date 2018/11/13 0013 16:59
     */
    public static void loadCache() {
        Circle circle = new Circle();
        circle.setId("1");
        System.out.println(circle);
        shapeMap.put(circle.getId(), circle);

        Square square = new Square();
        square.setId("2");
        System.out.println(square);
        shapeMap.put(square.getId(), square);

        Rectangle rectangle = new Rectangle();
        rectangle.setId("3");
        System.out.println(rectangle);
        shapeMap.put(rectangle.getId(), rectangle);
    }
}