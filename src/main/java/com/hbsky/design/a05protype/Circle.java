package com.hbsky.design.a05protype;

/**
 * 圆形
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:58
 */
public class Circle extends AbstractShape {

    public Circle() {
        type = "圆形";
    }

    @Override
    public void draw() {
        System.out.println("Inside 圆形::draw() method.");
    }
}