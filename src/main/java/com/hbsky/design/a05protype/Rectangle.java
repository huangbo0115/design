package com.hbsky.design.a05protype;

/**
 * 长方形
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:57
 */
public class Rectangle extends AbstractShape {

    public Rectangle() {
        type = "长方形";
    }

    @Override
    public void draw() {
        System.out.println("Inside 长方形::draw() method.");
    }
}