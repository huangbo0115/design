package com.hbsky.design.a05protype;

/**
 * 正方形
 *
 * @author bo.huang update
 * @date 2018/11/13 0013 16:58
 */
public class Square extends AbstractShape {

    public Square() {
        type = "正方形";
    }

    @Override
    public void draw() {
        System.out.println("Inside 正方形::draw() method.");
    }
}