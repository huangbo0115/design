package com.hbsky.design;

import com.alibaba.fastjson.JSONObject;
import com.hbsky.design.a05protype.AbstractShape;
import com.hbsky.design.a05protype.Circle;
import com.hbsky.design.a05protype.ShapeCache;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 原型模式 测试
 *
 * @author bo.huang
 * @since 2018/11/13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class DemoTest05 {

    @Test
    public void test01() {
        //加载
        ShapeCache.loadCache();

        AbstractShape clonedShape = ShapeCache.getShape("1");
        System.out.println("Shape : " + clonedShape.getType());
        System.out.println(clonedShape);

        AbstractShape clonedShape2 = ShapeCache.getShape("2");
        System.out.println("Shape : " + clonedShape2.getType());
        System.out.println(clonedShape2);

        AbstractShape clonedShape3 = ShapeCache.getShape("3");
        System.out.println("Shape : " + clonedShape3.getType());
        System.out.println(clonedShape3);
    }

    @Test
    public void test02() {
        Circle circle = new Circle();
        circle.setId("5");
        System.out.println(circle);
        System.out.println(JSONObject.toJSONString(circle));
        //克隆
        Circle clone = (Circle) circle.clone();
        System.out.println(clone);
        System.out.println(JSONObject.toJSONString(clone));
    }
}
