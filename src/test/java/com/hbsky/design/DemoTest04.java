package com.hbsky.design;

import com.hbsky.design.a04builder.*;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 建造者模式测试类
 *
 * @author bo.huang
 * @since 2018/11/13 0013 11:07
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class DemoTest04 {

    @Test
    public void test01() {
        //根据套餐点
        MealBuilder mealBuilder = new MealBuilder();

        Meal vegMeal = mealBuilder.prepareVegMeal();
        System.out.println("Veg Meal");
        vegMeal.showItems();
        System.out.println("Total Cost: " +vegMeal.getCost());

        Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
        System.out.println("\n\nNon-Veg Meal");
        nonVegMeal.showItems();
        System.out.println("Total Cost: " +nonVegMeal.getCost());
    }

    @Test
    public void test02() {
        //自己搭配
        MealBuilder mealBuilder = new MealBuilder();
        Meal meal = mealBuilder.definedMeal();
        meal.addItem(new ChickenBurger()).addItem(new VegBurger()).addItem(new Coke());
        System.out.println("自己点单:");
        meal.showItems();
        System.out.println("Total Cost: " +meal.getCost());
    }

}
