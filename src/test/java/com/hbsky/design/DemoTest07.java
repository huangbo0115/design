package com.hbsky.design;

import com.hbsky.design.a07bridge.Circle;
import com.hbsky.design.a07bridge.GreenCircle;
import com.hbsky.design.a07bridge.RedCircle;
import com.hbsky.design.a07bridge.Shape;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 桥接模式测试
 *
 * @author bo.huang
 * @since 2018/11/13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class DemoTest07 {

    @Test
    public void test01() {
        Shape redCircle = new Circle(100,100, 10, new RedCircle());
        Shape greenCircle = new Circle(100,100, 10, new GreenCircle());

        redCircle.draw();
        greenCircle.draw();
    }
}
