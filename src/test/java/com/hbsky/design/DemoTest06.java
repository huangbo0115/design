package com.hbsky.design;

import com.hbsky.design.a06adapter.AudioPlayer;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 适配器模式 测试
 * @author bo.huang
 * @since 2018/11/13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class DemoTest06 {

    @Test
    public void test01() {
        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play("mp3", "beyond the horizon.mp3");
        audioPlayer.play("mp4", "alone.mp4");
        audioPlayer.play("vlc", "far far away.vlc");
        audioPlayer.play("avi", "mind me.avi");
    }
}
