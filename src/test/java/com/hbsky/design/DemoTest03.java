package com.hbsky.design;

import com.hbsky.design.a03singleton.SingleObject;
import com.hbsky.design.a03singleton.Singleton2;
import com.hbsky.design.a03singleton.Singleton3;
import com.hbsky.design.a03singleton.Singleton4;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 单例模式次测试类
 *
 * @author bo.huang
 * @since 2018/11/13 0013 11:07
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class DemoTest03 {

    @Test
    public void test01() {
        //不合法的构造函数
        //编译时错误：构造函数 SingleObject() 是不可见的
        //SingleObject object = new SingleObject();
        //获取唯一可用的对象
        SingleObject object = SingleObject.getInstance();
        //显示消息
        object.showMessage();
    }

    @Test
    public void test02() {
        log.info("单例测试开始");
        Singleton2 singleton = Singleton2.getSingleton();
        Singleton2 singleton1 = Singleton2.getSingleton();
        Singleton3 instance = Singleton3.getInstance();
        Singleton3 instance1 = Singleton3.getInstance();
        SingleObject instance2 = SingleObject.getInstance();
        Singleton4 instance4 = Singleton4.INSTANCE;
    }
}
