package com.hbsky.design;

import com.hbsky.design.a01factory.Client;
import com.hbsky.design.a01factory.Food;

/**
 * 工厂模式测试类
 * https://www.cnblogs.com/malihe/p/6891920.html
 * @author bo.huang
 * @since 2018/11/13 0013 10:38
 */
public class DemoTest01 {

    public static void main(String[] args) {
        Client client = new Client();
        Food a = client.get("A");
        Food b = client.get("B");
        Food c = client.get(null);
        System.out.println("a:"+a);
        System.out.println("b:"+b);
        System.out.println("c:"+c);
    }

}
